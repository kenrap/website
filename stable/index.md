---
title: Unofficial FreeBSD pkgbase repository for 13-STABLE
author: Mina Galić
lang: en
---

## Package Sets

* [FreeBSD:13:amd64](/stable/FreeBSD:13:amd64/)
* [FreeBSD:13:aarch64](/stable/FreeBSD:13:aarch64/)
* [FreeBSD:13:armv7](/stable/FreeBSD:13:armv7/)
* [FreeBSD:13:i386](/stable/FreeBSD:13:i386/)

## Configuration

Add a configuration file for the PkgBase repo: 

`/usr/local/etc/pkg/repos/base.conf`

```
# FreeBSD pkgbase repo

FreeBSD-base: {
  url: "https://alpha.pkgbase.live/stable/${ABI}/latest",
  signature_type: "pubkey",
  pubkey: "/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub",
  enabled: yes
}
```

Then put in place a copy of [our public key](alpha.pkgbase.live.pub): 

```
~ $ sudo fetch --output=/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub \
      https://alpha.pkgbase.live/alpha.pkgbase.live.pub
```
