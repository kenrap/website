---
title: Unofficial FreeBSD pkgbase repository for 13.x-RELEASE
author: Mina Galić
lang: en
---

## 13.2-RELEASE Package Sets

* [FreeBSD:13:amd64](/release/13.2/FreeBSD:13:amd64/)
* [FreeBSD:13:aarch64](/release/13.2/FreeBSD:13:aarch64/)
* [FreeBSD:13:armv7](/release/13.2/FreeBSD:13:armv7/)
* [FreeBSD:13:i386](/release/13.2/FreeBSD:13:i386/)

## Configuration

Add a configuration file for the PkgBase repo: 

`/usr/local/etc/pkg/repos/base.conf`

```
# FreeBSD pkgbase repo

FreeBSD-base: {
  url: "https://alpha.pkgbase.live/release/13.2/${ABI}/latest",
  signature_type: "pubkey",
  pubkey: "/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub",
  enabled: yes
}
```

Then put in place a copy of [our public key](alpha.pkgbase.live.pub): 

```
~ $ sudo fetch --output=/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub \
      https://alpha.pkgbase.live/alpha.pkgbase.live.pub
```
