---
title: Unofficial FreeBSD pkgbase repository for 14-CURRENT
author: Mina Galić
lang: en
---

## Package Sets

* [FreeBSD:14:amd64](/current/FreeBSD:14:amd64/)
* [FreeBSD:14:aarch64](/current/FreeBSD:14:aarch64/)
* [FreeBSD:14:armv7](/current/FreeBSD:14:armv7/)
* [FreeBSD:14:i386](/current/FreeBSD:14:i386/)

## Configuration

Add a configuration file for the PkgBase repo: 

`/usr/local/etc/pkg/repos/base.conf`

```
# FreeBSD pkgbase repo

FreeBSD-base: {
  url: "https://alpha.pkgbase.live/current/${ABI}/latest",
  signature_type: "pubkey",
  pubkey: "/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub",
  enabled: yes
}
```

Then put in place a copy of [our public key](/alpha.pkgbase.live.pub): 

```
~ $ sudo fetch --output=/usr/share/keys/pkg/trusted/alpha.pkgbase.live.pub \
      https://alpha.pkgbase.live/alpha.pkgbase.live.pub
```

